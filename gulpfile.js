var gulp = require('gulp'),
	stylus = require('gulp-stylus'),
	livereload = require('gulp-livereload'),
	connect = require('gulp-connect'),
	nib = require('nib'),
	cache = require('gulp-cache');

paths = { styles: './stylus/styles.styl', all_files: './**/*'}
// Get one .styl file and render
gulp.task('one', function () {

	gulp.src(paths.styles)
		.pipe(stylus({use: [nib()]}))
		.pipe(gulp.dest('./css/'))
		.pipe(connect.reload());
});

gulp.task('connect', function() {

	connect.server({
		root: '',
		livereload: true
	});
});

gulp.task('watch', function(){

	livereload.listen();
	gulp.watch([paths.all_files]).on('change', livereload.changed);
	gulp.watch([paths.styles], ['one']);
});

gulp.task('clear', function (done) {
  return cache.clearAll(done);
});

// Tasks to execute
gulp.task('default', ['one', 'watch', 'connect', 'clear']);



